FROM ubuntu:16.04

RUN apt-get update \
	&& apt-get install -y \
	python3-dev python3-pip \
	make golang libjpeg-turbo8-dev git \
	xvfb python-opengl python3-numpy
	
RUN pip3 install --upgrade pip

#the requirements.txt doesn't install the go-vnc driver properly
#RUN mkdir /app
#COPY ./requirements.txt /app
#RUN pip3 install -r /app/requirements.txt

RUN pip3 install pillow ipdb selenium h5py

RUN mkdir -p /openai \
	&& cd /openai \
	&& git clone https://github.com/openai/go-vncdriver.git \
	&& git clone https://github.com/openai/gym.git \
	&& git clone https://github.com/openai/universe.git \
	&& git clone https://github.com/keras-team/keras.git \
	&& git clone https://github.com/matthiasplappert/keras-rl.git \
	&& git clone https://github.com/Theano/Theano.git \
	&& git clone https://github.com/tensorflow/tensorflow.git
	
RUN apt-get update \
	&& apt-get install -y \
	libx11-dev libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev \
	libxxf86vm-dev libgl1-mesa-dev mesa-common-dev
	
RUN cd /openai/go-vncdriver \
	#&& git checkout -b v0.4.16 tags/v0.4.16 \
	&& python3 build.py \
	&& pip3 install -e .

#openai/gym v0.9.6 removed gym.benchmarks that breaks openai/universe
RUN cd /openai/gym \
	&& git checkout -b v0.9.5 tags/v0.9.5 \
	&& pip3 install -e '.[classic_control]'

RUN cd /openai/universe \
	&& git checkout -b v0.21.3 tags/v0.21.3 \
	&& pip3 install -e .

RUN cd /openai/keras \
	#Downgrade Keras version due to following error when using Keras 2.1.4
	# File "/usr/local/lib/python3.5/dist-packages/rl/callbacks.py", line 245, in on_step_end
	#    self.progbar.update((self.step % self.interval) + 1, values=values, force=True)
	#TypeError: update() got an unexpected keyword argument 'force'
	&& git checkout -b 2.1.3 tags/2.1.3 \
	&& pip3 install -e .

RUN cd /openai/keras-rl \
	&& git checkout -b v0.4.0 tags/v0.4.0 \
	&& pip3 install -e .

RUN cd /openai/Theano \
	&& git checkout -b rel-1.0.1 tags/rel-1.0.1 \
	&& pip3 install -e .

#RUN cd /openai/tensorflow \
#	&& git checkout -b v1.5.0 tags/v1.5.0 \
#	&& pip3 install -e .
RUN pip3 install -Iv tensorflow==1.5.0

WORKDIR /app
CMD ["tail", "-f", "/dev/null"]
