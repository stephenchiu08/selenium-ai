#uses gym v0.9.5
import sys

from gym import spaces
from universe.envs.vnc_env import VNCEnv

class SeleniumBaseEnv(VNCEnv):
	#required by gym.core v0.9.5
	metadata = {'render.modes': ['rgb_array']}
	action_space = None
	
	selenium_driver = None
	vnc_driver = None
	
	base_url = None
	reward_driver = None
	window_width = None
	window_height = None
	window_length = 1
	start_position_x = 0
	start_position_y = 0

	def __init__(self, **kwargs):
		print('__init__')
		self.action_space = kwargs.pop('action_space', None)

		self.selenium_driver = kwargs.pop('selenium_driver', None)
		self.vnc_driver = kwargs.pop('vnc_driver', None)
		self.reward_driver = kwargs.pop('reward_driver', None)

		self.base_url = kwargs.pop('base_url', None)
		self.window_width = kwargs.pop('window_width', None)
		self.window_height = kwargs.pop('window_height', None)
		self.window_length = kwargs.pop('window_length', 1)
		
		self.start_position_x = kwargs.pop('start_position_x', 0)
		self.start_position_x = kwargs.pop('start_position_x', 0)
	
	def set_base_url(self, url):
		self.base_url = url
		
	def set_window_size(self, width, height):
		self.window_width = width
		self.window_height = height
		self.selenium_driver.set_window_size(self.window_width, self.window_height)

	def get_window_size(self):
		return self.window_width, self.window_height

	def get_num_actions(self):
		return self.action_space.get_num_actions()

	#step(self, action): Step the environment by one timestep.
	#Accepts an action and returns a tuple (observation, reward, done, info)
	#action = [[("PointerEvent", x, y, buttonmask), ("KeyEvent", key, down)]]
	#when using KeyEvent, remember to do both down=True and down=False to simulate a keypress
	def _step(self, action):
		#print('_step')
		observation, reward, done, info = self._get_state(action)
		return observation, reward, done, info

	#reset(self): Reset the environment's state. Returns observation.
	def _reset(self):
		#print('_reset')
		if (self.base_url is None):
			raise ValueError('Starting web page url not set')

		self.selenium_driver.set_window_position(self.start_position_x, self.start_position_y)
		if (self.window_width is None and self.window_height is None):
			self.selenium_driver.implicitly_wait(30)
			self.selenium_driver.maximize_window() # Note: driver.maximize_window does not work on Linux selenium version v2, instead set window size and window position like driver.set_window_position(0,0) and driver.set_window_size(1920,1080)
		else:
			self.selenium_driver.set_window_size(self.window_width, self.window_height)		

		self._do_reset_page()
		self._do_reset_mouse_position()

		if (self.reward_driver is not None):
			self.reward_driver.reset()
		
		return self._get_state()

	def _do_reset_page(self):
		self.selenium_driver.get(self.base_url)
		self.selenium_driver.execute_script("var coordsDiv=document.createElement('div');document.body.appendChild(coordsDiv);coordsDiv.style.position='absolute';coordsDiv.style.right=0;coordsDiv.style.bottom=0;coordsDiv.style.backgroundColor='black';coordsDiv.style.color='green';coordsDiv.style.padding='10px';coordsDiv.style.border='solid 2px green';window.onmousemove=function(e){coordsDiv.innerText='('+e.clientX+','+e.clientY+')'};")
		self.selenium_driver.execute_script("var isWithinWindow=true;window.onmouseout=function(){isWithinWindow=false};window.onmouseover=function(){isWithinWindow=true};}")

		self.selenium_driver.refresh()

	def _do_reset_mouse_position(self):
		actions = self.action_space.reset_mouse_position()
		self._get_state(actions)
		
	#render(self, mode='human', close=False): Render one frame of the environment. 
	#The default mode will do something human friendly, such as pop up a window. 
	#Passing the close flag signals the renderer to close any such windows.
	def _render(self, mode='human', close=False):
		#print('_render')
		return

	def _get_state(self, actions={}):
		#print('_get_state')
		#print(actions)
		#returns a tuple 
		#https://github.com/openai/gym/blob/master/gym/core.py
		observation, reward, done, info = self.vnc_driver.step(actions)

		if (self.reward_driver is not None):
			reward, done = self.reward_driver.get_reward()

		return observation, reward, done, info
