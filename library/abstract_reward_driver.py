class AbstractRewardDriver(object):
    def reset(self):
        raise NotImplementedError()

    #returns a tuple (reward, done)
    def get_reward(self):
        raise NotImplementedError()