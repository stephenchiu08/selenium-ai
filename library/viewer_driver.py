#from https://www.gridlastic.com/python-code-example.html
import atexit, os, signal

from gym.envs.classic_control import rendering

class GymViewerDriver(object):
	viewerSession = None

	#singletone (not a typo)
	#http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
	__instance = None
	def __new__(cls):
		if GymViewerDriver.__instance is None:
			GymViewerDriver.__instance = object.__new__(cls)
			GymViewerDriver.cleanup()
		return GymViewerDriver.__instance

	def cleanup():
		if GymViewerDriver.__instance is not None and GymViewerDriver.__instance.viewerSession is not None:
			GymViewerDriver.__instance.viewerSession.close()
			GymViewerDriver.__instance.viewerSession = None

	def get_session(self):
		if self.viewerSession is None:
			self.viewerSession = self._create_session()

		return self.viewerSession
		
	def _create_session(self):
		return rendering.SimpleImageViewer()

@atexit.register
def gymViewerCleanup():
	GymViewerDriver.cleanup()
