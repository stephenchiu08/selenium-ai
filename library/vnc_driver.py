#from https://www.gridlastic.com/python-code-example.html
import atexit, os, signal

#import go_vncdriver
from universe.envs.vnc_env import VNCEnv

class VncDriver(object):
	vncSession = None
	
	vncName = 'chrome'
	vncAddress='chrome:5900'
	vncPassword='secret'

	#singletone (not a typo)
	#http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
	__instance = None
	def __new__(cls):
		if VncDriver.__instance is None:
			VncDriver.__instance = object.__new__(cls)
			VncDriver.cleanup()
		return VncDriver.__instance

	def cleanup():
		if VncDriver.__instance is not None and VncDriver.__instance.vncSession is not None:
			VncDriver.__instance.vncSession.close()
			VncDriver.__instance.vncSession = None

	def get_session(self):
		if self.vncSession is None:
			self.vncSession = self._create_session()

		return self.vncSession
		
	def _create_session(self):
		session = VNCEnv()
		session._configure(remotes='vnc://'+self.vncAddress, vnc_kwargs={'password': self.vncPassword})
		return session
	
		# session = go_vncdriver.VNCSession()
		# session.connect(name=self.vncName, address=self.vncAddress, password=self.vncPassword)	
		# return session

@atexit.register
def vncDriverCleanup():
	VncDriver.cleanup()
