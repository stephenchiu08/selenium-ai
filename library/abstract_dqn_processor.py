from rl.core import Processor

class AbstractDQNProcessor(Processor):
    def process_observation(self, new_observation):
        raise NotImplementedError()

    def process_state_batch(self, batch):
        raise NotImplementedError()

    def process_reward(self, reward):
        raise NotImplementedError()
