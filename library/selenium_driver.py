#from https://www.gridlastic.com/python-code-example.html
import atexit, os, signal
from selenium import webdriver

class SeleniumSessionDriver(object):
	seleniumSession = None
	seleniumHubUrl = "http://hub:4444/wd/hub"
	seleniumCapabilities = {
		"browserName": "chrome"
	}

	#singletone (not a typo)
	#http://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
	__instance = None
	def __new__(cls):
		if SeleniumSessionDriver.__instance is None:
			SeleniumSessionDriver.__instance = object.__new__(cls)
			SeleniumSessionDriver.cleanup()
		return SeleniumSessionDriver.__instance

	def cleanup():
		if SeleniumSessionDriver.__instance is not None and SeleniumSessionDriver.__instance.seleniumSession is not None:
			SeleniumSessionDriver.__instance.seleniumSession.quit()
			SeleniumSessionDriver.__instance.seleniumSession = None

	def get_session(self):
		if self.seleniumSession is None:
			self.seleniumSession = self._create_session()
			#self.seleniumSession.implicitly_wait(30)
			#self.seleniumSession.maximize_window() # Note: driver.maximize_window does not work on Linux selenium version v2, instead set window size and window position like driver.set_window_position(0,0) and driver.set_window_size(1920,1080)

		return self.seleniumSession
		
	def _create_session(self):
		options = webdriver.ChromeOptions()
		options.add_argument("--disable-infobars")
		caps = options.to_capabilities()

		driver = webdriver.Remote(
		   command_executor= self.seleniumHubUrl,
		   desired_capabilities=caps
		)
		return driver

@atexit.register
def seleniumCleanup():
	SeleniumSessionDriver.cleanup()
