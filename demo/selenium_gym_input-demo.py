#xvfb-run -s "-screen 0 1400x900x24" python3 /app/demo/selenium_gym_input-demo.py

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import gym
import time

from universe import spaces
from demo.envs.selenium_gym_input_demo_env import SeleniumGymInputDemoEnv

env = gym.make('mf.SeleniumGymInputDemo-v0')
env.set_base_url('http://unixpapa.com/js/testkey.html')
env.reset()
for i in range(97, 123):
	print('pressing character ' + chr(i))
	actions = [[
		('KeyEvent', chr(i), True),
		('KeyEvent', chr(i), False),
		#spaces.KeyEvent.by_name(chr(i), down=True),
		#spaces.KeyEvent.by_name(chr(i), down=False),
	]]
	observation, reward, done, info = env.step(actions)
	time.sleep(0.2)
time.sleep(5)

env.set_base_url('http://unixpapa.com/js/mousecoord.html')
env.reset()
actions = env.action_space.reset_mouse_position()
env.step(actions)
actions = env.action_space.mouse_press()
env.step(actions)
for i in range(10):
	print('moving mouse up')
	actions = env.action_space.move_mouse_up()
	observation, reward, done, info = env.step(actions)
	actions = env.action_space.mouse_press()
	observation, reward, done, info = env.step(actions)
	time.sleep(0.2)

time.sleep(5)
