#from https://www.gridlastic.com/python-code-example.html
#refactored to include the selenium driver in a separate file for reuse

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from library.selenium_driver import SeleniumSessionDriver
from selenium.webdriver.common.keys import Keys

driver = SeleniumSessionDriver().get_session()
try:
    driver.get("http://www.python.org")
    assert "Python" in driver.title
    elem = driver.find_element_by_name("q")
    elem.send_keys("documentation")
    elem.send_keys(Keys.RETURN)
    assert "No results found." not in driver.page_source
finally:
	pass
