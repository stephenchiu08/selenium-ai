from gym.envs.registration import register

register(
    id='mf.SeleniumGymInputDemo-v0',
    entry_point='demo.envs:SeleniumGymInputDemoEnv',
)