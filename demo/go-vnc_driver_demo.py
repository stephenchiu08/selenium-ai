#https://github.com/openai/go-vncdriver/blob/master/examples/usage.py
#https://github.com/openai/go-vncdriver/blob/master/tests/test_smoke.py
#xvfb-run -s "-screen 0 1400x900x24" python3 /app/demo/go-vnc_driver_demo.py

import os
import sys
import calendar
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from library.viewer_driver import GymViewerDriver
from library.vnc_driver import VncDriver

from PIL import Image

viewer = GymViewerDriver().get_session()
session = VncDriver().get_session()
for i in range(10):
	observations, infos, errors = session.step({})
	try:
		if errors.get('chrome'):
			print("error", errors.get('chrome'))
		elif any(v['stats.vnc.updates.n'] for k, v in infos.items()):
			for conn, value in observations.items():
				if value is not None:
					#print(i, value)
					viewer.imshow(value)
					image = Image.fromarray(value)
					image.save('/app/screenshots/vncdriver-demo-'+ str(calendar.timegm(time.gmtime()))+'.jpg', 'JPEG')
					#print(value.shape)
	except Exception as e:
		print(e)

	time.sleep(0.2)