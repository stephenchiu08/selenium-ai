#Selenium-AI

This purpose of this project is to create a docker environment where reinforcement learning is used to play [Tic-Tac-Toe](https://playtictactoe.org) with minimal instructions.

##File structure
* demo - contains little building blocks to get the tictactoe environment running
* library - common code that could be reusable for different environments
* screenshots - place where screenshots are placed
* tictactoe - contains the code to run the tictactoe machine learning
* docker-compose.yml - Docker compose file
* Dockerfile - specifies the requirements for the python reinforcement learning environment

##Steps taken so far
1. Created environment to run Python3 code
2. Add Selenium Hub docker environment
3. Add a building block demo of Python3 utilizing Selenium Hub based on this [code](https://www.gridlastic.com/python-code-example.html). To run this demo code, use: python3 /app/demo/selenium_demo.py
4. Update docker file to allow for the creation of a building block demo of [OpenAI's VNC Driver](https://github.com/openai/go-vncdriver). This is used in preparation of Open AI being aware of its web environment. To run this demo code, use: xvfb-run -s "-screen 0 1400x900x24" python3 ./demo/go-vnc_driver_demo.py
5. Create a sample environment ('mf.SeleniumGymDemo-v0') that makes uses of the Selenium Hub and the above Go-VNC driver
6. Load up the tictactoe environment
7. Define the action space
8. Define the rewards/penalties
9. Break down example Keras-RL Atari environment into distinct parts for demo.
10. To run the tictactoe demo, use: xvfb-run -s "-screen 0 600x600x24" python3 /app/tictactoe/tictactoe.py

##Machine learning terminology
###2D convolution layer (e.g. spatial convolution over images).
[Convolutional neural network](https://en.wikipedia.org/wiki/Convolutional_neural_network) is a machine learning model based on how the visual cortex of the brain processes images. The visual cortex breaks down information from the eye by having a groups of neurons process a small piece of the image. The output of these groups of neurons are then processed through 6 distinct layers - V1 to V6. Each of these layers are in charge of detecting a specific attribute of the image such as, pattern recognition (V1), color (V4), motion (V6). 

####Convolutional neural network spatial arragement
Stride controls how much overlapping there is between the each section of the neural network.

##Current issues encountered
* The tictactoe demo will throw an error by Tensorflow reporting an out of memory issue.
* Installation of packages via requirements.txt file doesn't install the go-vnc driver correctly (Python won't detect it)

##Current docker compose environment
* Custom built Python3 environment due to trouble setting up go-vnc on Docker hub's [3.4.7-jessie](https://hub.docker.com/_/python/) build.
* [Selenium Hub](https://hub.docker.com/r/selenium/hub/) with [Chrome debug node]() for vnc visualisation

##Resources/inspiration
* [SeleniumAI](https://github.com/bewestphal/SeleniumAI)
* [Selenium API](http://selenium-python.readthedocs.io/api.html)
* [Keras RL](https://github.com/matthiasplappert/keras-rl)
* [Keras RL Docs](https://keras-rl.readthedocs.io/en/latest/agents/overview/)
* [OpenAI Go-VNC](https://github.com/openai/go-vncdriver) v0.4.16
* [OpenAI Gym](https://github.com/openai/gym) v0.9.5
* [OpenAI Universe](https://github.com/openai/gym) v0.21.3
* [OpenAI environment example](https://github.com/MartinThoma/banana-gym)
* [TensorFlow](https://www.tensorflow.org/get_started/get_started)
* [Convolution explanation](http://setosa.io/ev/image-kernels)

