import keras.backend as K

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Conv2D, Permute

class TicTacToeModel(object):
    model = None
    input_shape = None
    num_actions = None

    def __init__(self, *args, **kwargs):
        self.input_shape = kwargs.pop('input_shape', None)
        assert self.input_shape is not None
        self.num_actions = kwargs.pop('num_actions', None)
        assert self.input_shape is not None
        self.model = kwargs.pop('model', Sequential())
        assert self.model is not None

        self._init_model()

    def _init_model(self):
        image_dim_ordering = K.image_dim_ordering()

        if image_dim_ordering == 'tf':
            # (width, height, channels)
            self.model.add(Permute((2, 3, 1), input_shape=self.input_shape))
        elif image_dim_ordering == 'th':
            # (channels, width, height)
            self.model.add(Permute((1, 2, 3), input_shape=self.input_shape))
        else:
            raise RuntimeError('Unknown image_dim_ordering.')
        
        #2D convolution layer (e.g. spatial convolution over images).
        #kernel_size: An integer or tuple/list of 2 integers, specifying the width and height of the 2D convolution window. Can be a single integer to specify the same value for all spatial dimensions.
        #strides: An integer or tuple/list of 2 integers, specifying the strides of the convolution along the width and height. Stride controls how depth columns around the spatial dimensions (width and height) are allocated. When the stride is 1 then we move the filters one pixel at a time. This leads to heavily overlapping receptive fields between the columns, and also to large output volumes. When the stride is 2 (or rarely 3 or more) then the filters jump 2 pixels at a time as they slide around. The receptive fields overlap less and the resulting output volume has smaller spatial dimensions.
        #self.model.add(Convolution2D(32, 8, 8, subsample=(4, 4)))
        self.model.add(Conv2D(32, (8, 8), strides=(4, 4)))
        #ReLU layer abbreviation of Recified Linear Units

        self.model.add(Activation('relu'))

        #self.model.add(Convolution2D(64, 4, 4, subsample=(2, 2)))
        self.model.add(Conv2D(64, (4, 4), strides=(2, 2)))
        self.model.add(Activation('relu'))

        #self.model.add(Convolution2D(64, 3, 3, subsample=(1, 1)))
        self.model.add(Conv2D(64, (3, 3), strides=(1, 1)))
        self.model.add(Activation('relu'))

        #Flattens the input. Does not affect the batch size.
        self.model.add(Flatten())

        #Just your regular densely-connected NN layer.
        self.model.add(Dense(512))
        self.model.add(Activation('relu'))

        self.model.add(Dense(self.num_actions))
        self.model.add(Activation('linear'))

    def get_model(self):
        return self.model