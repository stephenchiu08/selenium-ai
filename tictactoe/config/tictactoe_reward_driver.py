from library.abstract_reward_driver import AbstractRewardDriver

class TicTacToeRewardDriver(object):
    selenium_driver = None
    action_space = None

    old_url = None
    old_player_score = None
    old_tie_score = None
    old_computer_score = None

    current_url = None
    current_player_score = None
    current_tie_score = None
    current_computer_score = None

    def __init__(self, *args, **kwargs):
        self.selenium_driver = kwargs.pop('selenium_driver', None)
        self.action_space = kwargs.pop('action_space', None)

    def reset(self):
        self.old_url = self.current_url = self._get_current_url()
        
        player_score, tie_score, computer_score = self._get_current_score()
        self.old_player_score = self.current_player_score = player_score
        self.old_tie_score = self.current_tie_score = tie_score
        self.old_computer_score = self.current_computer_score = computer_score

    def get_reward(self):
        reward = 0
        done = False
        self._update_current_state()

        if self.old_url is not None:
            #clicked on ad. this is bad
            if self._has_url_changed():
                reward = -1.
            #detect if the mouse it still on the web page
            elif (self._is_out_of_bound()):
                reward = -1.
            #neuro network won. yay!
            elif self._did_p1_win():
                reward = 1.
            #not ideal, but better than losing
            elif self._did_p1_tie_p2():
                reward = 0.5
            #losing is bad
            elif self._did_p2_win():
                reward = -1.
            
            done = self._is_done()
        '''
        print({
            'action_space': {
                'out_of_bounds': self._is_out_of_bound(),
                'x': self.action_space.mouse_position_x,
                'y': self.action_space.mouse_position_y,
            },
            'player': {
                'old': self.old_player_score,
                'new': self.current_player_score,
            },
            'tie': {
                'old': self.old_tie_score,
                'new': self.current_tie_score,
            },
            'computer': {
                'old': self.old_computer_score,
                'new': self.current_computer_score,
            },
            'result': {
                'reward': reward, 
                'done': done
            }
        })
        '''
        self._copy_current_state_to_old_state()

        return reward, done

    def _is_done(self):
        return self._has_url_changed() or self._is_out_of_bound() or self._did_p1_win() or self._did_p1_tie_p2() or self._did_p2_win()

    def _is_out_of_bound(self):
        return not self.selenium_driver.execute_script('try{return document.isWithinWindow}catch(e){return false}')

    def _update_current_state(self):
        self.current_url = self._get_current_url()
        self.current_player_score, self.current_tie_score, self.current_computer_score = self._get_current_score()

    def _copy_current_state_to_old_state(self):
        self.old_player_score = self.current_player_score
        self.old_tie_score = self.current_tie_score
        self.old_computer_score = self.current_computer_score

    def _get_current_score(self):
        player_score = int(self.selenium_driver.find_element_by_css_selector('.scores .player1 .score').text)
        tie_score = int(self.selenium_driver.find_element_by_css_selector('.scores .ties .score').text)
        computer_score = int(self.selenium_driver.find_element_by_css_selector('.scores .player2 .score').text)		

        return player_score, tie_score, computer_score

    def _get_current_url(self):
        return self.selenium_driver.current_url

    def _has_url_changed(self):
        return self.current_url != self.old_url

    def _has_score_changed(self):
        return self._did_p1_win() or self._did_p1_tie_p2() or self._did_p2_win()

    def _did_p1_win(self):
        return self.current_player_score != self.old_player_score

    def _did_p2_win(self):
        return self.current_computer_score != self.old_computer_score

    def _did_p1_tie_p2(self):
        return self.current_tie_score != self.old_tie_score
