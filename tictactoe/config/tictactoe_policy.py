#from rl.policy import LinearAnnealedPolicy, BoltzmannQPolicy, EpsGreedyQPolicy
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy

class TicTacToePolicy (object):
    policy = None

    def __init__(self, *args, **kwargs):    
        # Select a policy. We use eps-greedy action selection, which means that a random action is selected
        # with probability eps. We anneal eps from 1.0 to 0.1 over the course of 1M steps. This is done so that
        # the agent initially explores the environment (high eps) and then gradually sticks to what it knows
        # (low eps). We also set a dedicated eps value that is used during testing. Note that we set it to 0.05
        # so that the agent still performs some random actions. This ensures that the agent cannot get stuck.
        self.policy = LinearAnnealedPolicy(
            EpsGreedyQPolicy(), 
            attr='eps', 
            value_max=1., 
            value_min=.1, 
            value_test=.05,
            nb_steps=1000000
        )

    def get_policy(self):
        return self.policy