from library.abstract_dqn import AbstractDQN

import os

import gym
from rl.agents.dqn import DQNAgent
from keras.optimizers import Adam

from rl.callbacks import FileLogger, ModelIntervalCheckpoint

from tictactoe.config.tictactoe_model import TicTacToeModel
from tictactoe.config.tictactoe_memory import TicTacToeMemory
from tictactoe.config.tictactoe_processor import TicTacToeProcessor
from tictactoe.config.tictactoe_policy import TicTacToePolicy

class TicTacToeDQN(AbstractDQN):
    env = None
    model = None
    memory = None
    processor = None
    dqn = None

    dqn_file = None

    nb_steps = 2000
    #decrease training iterations due to memory leak. possibly caused by tensorflow?
    #training_iterations = 875
    training_iterations = 10

    def __init__(self, *args, **kwargs):
        self.dqn_file = kwargs.pop('selenium_web_game', 'mf.TicTacToe-v0')
        self.weights_filename = 'dqn_{}_weights.h5f'.format(self.dqn_file)
        self.env = gym.make(self.dqn_file)
        self.env.set_window_size(
            width = kwargs.pop('window_width', 1),
            height = kwargs.pop('window_height', 1)
        )

        window_length = kwargs.pop('window_length', 1)
        window_size = self.env.get_window_size()
        input_shape = (window_length,) + window_size
        num_actions = self.env.get_num_actions()

        self.dqn = DQNAgent(
            model = TicTacToeModel(
                input_shape = input_shape,
                num_actions = num_actions,
            ).get_model(), 
            nb_actions=num_actions, 
            policy = TicTacToePolicy().get_policy(),
            memory = TicTacToeMemory(
                    window_length = window_length
                ).get_memory(),
            processor = TicTacToeProcessor(
                input_shape = window_size
            ).get_processor(), 
            nb_steps_warmup=50000,
            gamma=.99, 
            target_model_update=10000,
            train_interval=4, 
            delta_clip=1.
        )

        self.dqn.compile(Adam(lr=.00025), metrics=['mae'])

    def do_training(self):
        #A callback is a set of functions to be applied at given stages of the training procedure. You can use callbacks to get a view on internal states and statistics of the model during training. 
        callbacks = []

        #load the weights
        if (os.path.isfile(self.weights_filename)):
            try:
                print('Loading existing weights file: '+self.weights_filename)
                self.dqn.load_weights(self.weights_filename)
                print ('Weight file loaded')
            except:
                print('Failed loading existing weights file')

        for i in range(1, self.training_iterations):
            print('Starting iteration - {}'.format(i))
            #callbacks (list of keras.callbacks.Callback or rl.callbacks.Callback instances): List of callbacks to apply during training. See callbacks for details.
            #nb_steps (integer): Number of training steps to be performed.
            #log_interval (integer): If verbose = 1, the number of steps that are considered to be an interval.
            self.dqn.fit(self.env, callbacks=callbacks, nb_steps=self.nb_steps, log_interval=self.nb_steps)
            print('Saving iteration - {}'.format(i))
            self.dqn.save_weights(self.weights_filename, overwrite=True)
            print('Completed iteration - {}'.format(i))

    
    def do_test(self):
        #load the weights
        if (os.path.isfile(self.weights_filename)):
            print('Loading existing weights file: '+self.weights_filename)
            self.dqn.load_weights(self.weights_filename)
        else:
            print('No existing weights file loaded. Testing with no previous knowledge')
        self.dqn.test(self.env, nb_episodes=10, visualize=False)
