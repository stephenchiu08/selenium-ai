from rl.memory import SequentialMemory

class TicTacToeMemory(object):
    memory = None

    def __init__(self, *args, **kwargs):
        self.memory = SequentialMemory(
            limit=1000000, 
            window_length=kwargs.pop('window_length', None),
        )
    
    def get_memory(self):
        return self.memory