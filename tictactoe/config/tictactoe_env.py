from library.selenium_environment import SeleniumBaseEnv
from library.action_space import ActionSpace
from library.selenium_driver import SeleniumSessionDriver
from library.vnc_driver import VncDriver

from tictactoe.config.tictactoe_reward_driver import TicTacToeRewardDriver

class TicTacToeEnv(SeleniumBaseEnv):
	tic_tac_toe_url = 'https://playtictactoe.org/'

	def __init__(self, **kwargs):
		#print('__init__ tictactoe')
		selenium_driver = SeleniumSessionDriver().get_session()
		action_space = ActionSpace(driver=selenium_driver)
		base_url = kwargs.pop('base_url', self.tic_tac_toe_url)
		window_width = kwargs.pop('window_width', 600)
		window_height = kwargs.pop('window_height', 600)
		window_length = kwargs.pop('window_length', 1)

		super(TicTacToeEnv, self).__init__(
			selenium_driver = selenium_driver,
			vnc_driver = VncDriver().get_session(),

			action_space = action_space,
			base_url = base_url,
			window_width = window_width,
			window_height = window_height,
			window_length = window_length,

			reward_driver = TicTacToeRewardDriver(
				selenium_driver=selenium_driver,
				action_space=action_space,
			),
		)
	
	#retain current score, override parent _do_reset_page method
	def _do_reset_page(self):
		if self.selenium_driver.current_url != self.tic_tac_toe_url:
			self.selenium_driver.get(self.base_url)
			self.selenium_driver.execute_script("var coordsDiv=document.createElement('div');document.body.appendChild(coordsDiv);coordsDiv.style.position='absolute';coordsDiv.style.right=0;coordsDiv.style.bottom=0;coordsDiv.style.backgroundColor='black';coordsDiv.style.color='green';coordsDiv.style.padding='10px';coordsDiv.style.border='solid 2px green';window.onmousemove=function(e){coordsDiv.innerText='('+e.clientX+','+e.clientY+')'};")
			self.selenium_driver.execute_script("document.isWithinWindow=true;window.onmouseout=function(){document.isWithinWindow=false};window.onmouseover=function(){document.isWithinWindow=true}")
			self.selenium_driver.execute_script("document.querySelector('.scores').onclick=function(){}")

	def _do_reset_mouse_position(self):
		player_score = int(self.selenium_driver.find_element_by_css_selector('.scores .player1 .score').text)
		tie_score = int(self.selenium_driver.find_element_by_css_selector('.scores .ties .score').text)
		computer_score = int(self.selenium_driver.find_element_by_css_selector('.scores .player2 .score').text)		

		actions = self.action_space.reset_mouse_position()
		self._get_state(actions)

		if player_score + tie_score + computer_score > 0:
			actions = self.action_space.mouse_press()
			self._get_state(actions)

	def _get_state(self, actions={}):
		#print('_get_state tictactoeenv')
		#map the action number with the action map
		try:
			actions = self.action_space.available_actions[actions]()
		except:
			pass

		observation, reward, done, info = super()._get_state(actions=actions)

		return observation, reward, done, {}
