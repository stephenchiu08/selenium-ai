from gym.envs.registration import register

register(
    id='mf.TicTacToe-v0',
    entry_point='tictactoe.config:TicTacToeEnv',
)