#https://keras.io/
#based off of https://github.com/matthiasplappert/keras-rl/blob/master/examples/dqn_atari.py 
#xvfb-run -s "-screen 0 600x600x24" python3 /app/tictactoe/tictactoe.py


import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import argparse

from tictactoe.config.tictactoe_dqn import TicTacToeDQN

parser = argparse.ArgumentParser(description='Run the tic-tac-toe simulator.')
print(dir(parser))
parser.add_argument('--test', action="store_true", help='Run the test based on previous training sessions')
args = parser.parse_args()

tictactoe_dqn = TicTacToeDQN(
    selenium_web_game = 'mf.TicTacToe-v0',

    window_width=600,
    window_height=600,
)

if (args.test):
    tictactoe_dqn.do_test()
else:
    tictactoe_dqn.do_training()